/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'noise-app',
    environment: environment,
    rootURL: '/',
    defaultLocationType: 'auto',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
      }
    },

    contentSecurityPolicy: {
      'script-src': '\'self\' \'unsafe-eval\' apis.google.com',
      'style-src': '\'self\' \'unsafe-inline\' fonts.googleapis.com',
      'font-src': '\'self\' fonts.gstatic.com',
      'frame-src': '\'self\' https://*.firebaseapp.com',
      'img-src': '\'self\' *.gravatar.com s3.amazonaws.com',
      'connect-src': '\'self\' wss://*.firebaseio.com https://*.googleapis.com'
    },

    firebase: {
      apiKey: "AIzaSyC95H2HkzsPM336k659uh5AxKI-pXVtUx8",
      authDomain: "soundbigdata.firebaseapp.com",
      databaseURL: "https://soundbigdata.firebaseio.com",
      storageBucket: "soundbigdata.appspot.com",
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },

    moment: {
      // Options:
      // 'all' - all years, all timezones
      // '2010-2020' - 2010-2020, all timezones
      // 'none' - no data, just timezone API
      includeTimezone: 'all'
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
