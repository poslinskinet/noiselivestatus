import Ember from 'ember';

export default Ember.Component.extend({
  audioContext: new AudioContext(),
  BUFF_SIZE: 16384,

  arrayBuffer: [0,0,0,0,0,0,0,0,0,0],
  value: 0,
  valueChanged: Ember.observer('value', function() {
    let arrayValue = this.get('arrayBuffer');
    arrayValue.shift();
    arrayValue.push(this.get('value'));
    this.set('arrayBuffer', arrayValue);
    this.get('measure').set('value', this.get('arrayBuffer'));
    this.get('measure').save();
  }),

  computaBuffa: Ember.computed('measure.value', function() {
    return this.get('measure.value');
  }),

  measure: Ember.computed('measures.[]', function() {
    return this.get('measures.firstObject');
  }),

  didInsertElement() {
    if (!navigator.getUserMedia) {
      navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia || navigator.msGetUserMedia;
    }

    if (navigator.getUserMedia){
        navigator.getUserMedia({audio:true},
          (stream) => {
              this.start_microphone(stream);
          },
          function() {
            alert('Error capturing audio.');
          }
        );
    }
    else
    {
      alert('getUserMedia not supported in this browser.');
    }
  },

  process_microphone_buffer(event) {
      var microphone_output_buffer;
      microphone_output_buffer = event.inputBuffer.getChannelData(0);
      this.show_some_data(microphone_output_buffer, 1000);
  },

  start_microphone(stream){
    this.set('gain_node', this.get('audioContext').createGain());
    this.get('gain_node').connect( this.get('audioContext').destination );

    this.set('microphone_stream', this.get('audioContext').createMediaStreamSource(stream));
    this.get('microphone_stream').connect(this.get('gain_node'));

    this.set('script_processor_node', this.get('audioContext').createScriptProcessor(this.BUFF_SIZE, 1, 1));
    // this.get('script_processor_node').onaudioprocess = this.process_microphone_buffer;

    this.get('microphone_stream').connect(this.get('script_processor_node'));

    this.set('script_processor_fft_node', this.get('audioContext').createScriptProcessor(2048, 1, 1));
    this.get('script_processor_fft_node').connect(this.get('gain_node'));

    this.set('analyserNode', this.get('audioContext').createAnalyser());
    this.get('analyserNode').smoothingTimeConstant = 0;
    this.get('analyserNode').fftSize = 2048;

    this.get('microphone_stream').connect(this.get('analyserNode'));

    this.get('analyserNode').connect(this.get('script_processor_fft_node'));

    this.get('script_processor_fft_node').onaudioprocess = () => {

      // get the average for the first channel
      var array = new Uint8Array(this.get('analyserNode').frequencyBinCount);
      this.get('analyserNode').getByteFrequencyData(array);

      // draw the spectrogram
      if (this.get('microphone_stream').playbackState === this.get('microphone_stream').PLAYING_STATE) {
          this.show_some_data(array, 10);
      }
    };
  },

  show_some_data(given_typed_array, num_row_to_display) {
      var size_buffer = given_typed_array.length;
      var index = 0;
      var max_index = num_row_to_display;

      for (; index < max_index && index < size_buffer; index += 1) {
        let percentage = given_typed_array[index]*100;
        this.$('#value').css({width: percentage/255+'%'});
        this.$('input').val(percentage).change();
        this.set('value', percentage);
      }
  },

  actions: {
    soundChange() {

    }
  }
});
