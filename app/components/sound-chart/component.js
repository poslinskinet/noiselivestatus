import Ember from 'ember';

export default Ember.Component.extend({
  measure: Ember.computed.alias('measures.firstObject'),

  chartOptions: Ember.computed('measure.value', function() {
    return {
      title: { text: '' },
      xAxis: {
        type: 'datetime',
        title: { text: 'Date' },
      },
      yAxis: {
        title: { text: 'Noise' },
        min: 0,
        max: 25000,
      }
    };
  }),

  chartData: Ember.computed('measure.value', function() {
    return [{
      name: 'Noise level',
      data: this.data(),
    }];
  }),

  data() {
    if ( !this.get('measure.value') ) {
      return [];
    }
    return this.get('measure.value').map(function(value) {
      return [new Date(), value];
    });
  }
});
