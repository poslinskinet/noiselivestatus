import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  offset: 10,
  maxWeight: Ember.computed('weight', function() {
    return this.get('weight.value') + this.get('offset');
  }),
  minWeight: Ember.computed('weight', function() {
    return this.get('weight.value') - this.get('offset');
  }),

  weight: Ember.computed('weights', function() {
    if ( new Date(this.lastWeight().get('createdAt')).toLocaleDateString() === this.today() )
    {
      return this.lastWeight();
    }

    return this.newWeight();
  }),

  prettyValue: Ember.computed('weight.value', function() {
    return parseFloat(this.get('weight.value')).toFixed(1);
  }),

  today() {
    let date = new Date();
    date.setHours(0, 0, 0, 0, 0);
    return date.toLocaleDateString();
  },

  lastWeight() {
    return this.get('weights.lastObject');
  },

  newWeight() {
    let newRecord = this.get('store').createRecord('weight', {});
    newRecord.set('value', this.lastWeight().get('value'));
    return newRecord;
  },

  actions: {
    dateSelected(date) {
      this.get('weight').set('createdAt', date);
    },

    save() {
      let fixedValue = parseInt(this.get('weight.value') * 10);
      this.set('weight.value', fixedValue / 10);
      this.get('weight').save();

      this.sendAction('afterSave');
    },
  }
});
